package pe.alicorp.services.bridgeazuredirectory.proxy;

import pe.alicorp.services.bridgeazuredirectory.model.request.BridgeRequest;
import pe.alicorp.services.bridgeazuredirectory.model.response.BridgeSuccesResponse;
import reactor.core.publisher.Mono;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
public interface AzureCloudProxy {
	
	@POST("/{tenantId}/oauth2/token")
	@Headers("Content-Type: application/json")
	Mono<BridgeSuccesResponse> requestToAzure(@Body BridgeRequest request, @Path("tenantId") String tenantId);
	

}
