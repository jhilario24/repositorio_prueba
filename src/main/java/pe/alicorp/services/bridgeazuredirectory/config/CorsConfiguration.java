package pe.alicorp.services.bridgeazuredirectory.config;

import org.springframework.web.reactive.config.CorsRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;

public class CorsConfiguration implements WebFluxConfigurer{
	  @Override
	  public void addCorsMappings(CorsRegistry corsRegistry) {
	    corsRegistry.addMapping("/**")
	        .allowedOrigins("*")
	        .allowedHeaders("*")
	        .exposedHeaders("search.hits")
	        .allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS")
	        .maxAge(3600);
	  }
}
