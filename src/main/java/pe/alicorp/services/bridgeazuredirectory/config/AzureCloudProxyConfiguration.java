package pe.alicorp.services.bridgeazuredirectory.config;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;
import pe.alicorp.altemista.core.http.utils.AltemistaHttpClient;
import pe.alicorp.services.bridgeazuredirectory.proxy.AzureCloudProxy;

@Slf4j
@Configuration
public class AzureCloudProxyConfiguration {

  @Bean
  AzureCloudProxy azureCloudProxy(
      @Value("${application.proxy.azure.base-url}") String baseUrl)//,
//      @Value("${application.proxy.sap.p12-path}") String p12Path,
//      @Value("${application.proxy.sap.p12-password}") String p12Password)
      throws NoSuchAlgorithmException, KeyManagementException, IOException, CertificateException,
      KeyStoreException, UnrecoverableKeyException {
//    final TrustManager[] trustAllCerts =  this.trustAllCerts();

//    KeyManager[] keyManagers;
//    try (InputStream inputStream = getClass().getResourceAsStream(p12Path)) {
//      KeyStore keyStore = KeyStore.getInstance("PKCS12");
//      keyStore.load(inputStream, p12Password.toCharArray());
//      final KeyManagerFactory instance = KeyManagerFactory.getInstance("SunX509");
//      instance.init(keyStore, p12Password.toCharArray());
//      keyManagers = instance.getKeyManagers();
//    }

//    SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
//    sslContext.init(keyManagers, trustAllCerts, null);

//    final OkHttpClient.Builder builder = new OkHttpClient.Builder()
//        .connectionSpecs(Collections.singletonList(MODERN_TLS))
//        .hostnameVerifier((host, session) -> true)
//        .sslSocketFactory(sslContext.getSocketFactory(), (X509TrustManager) trustAllCerts[0]);

    return AltemistaHttpClient.builder()
//        .clientBuilder(builder)
        .baseUrl(baseUrl)
        .buildProxy(AzureCloudProxy.class);
  }

//  private TrustManager[] trustAllCerts() {
//    return new TrustManager[] { new X509TrustManager() {
//      @Override
//      public void checkClientTrusted(X509Certificate[] chain, String authType)  {
//      }
//
//      @Override
//      public void checkServerTrusted(X509Certificate[] chain, String authType)  {
//      }
//
//      @Override
//      public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//        return new X509Certificate[] {};
//      }
//    }};
//  }
}
