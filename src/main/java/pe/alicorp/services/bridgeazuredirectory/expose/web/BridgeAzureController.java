package pe.alicorp.services.bridgeazuredirectory.expose.web;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pe.alicorp.services.bridgeazuredirectory.model.request.BridgeRequest;
import pe.alicorp.services.bridgeazuredirectory.model.response.BridgeSuccesResponse;
import pe.alicorp.services.bridgeazuredirectory.service.BridgeAzureService;
import reactor.core.publisher.Mono;

@RestController

@Slf4j
@RequestMapping("/portal/active-directory/v1")
@AllArgsConstructor
public class BridgeAzureController {
	private BridgeAzureService bridgeService;
	
	@PostMapping("/token")
	public Mono<BridgeSuccesResponse> responseFromAzure(@RequestBody BridgeRequest request){
		log.info("Request enviado : --> {}", request);
		return bridgeService.demandAzure(request)
				.doOnSuccess(response ->{
					log.info("Entry to Succes {}",response);
				response.setAccessToken("Prueba Token");
				response.setExpires_on("Expiración");
	              })
				.doOnError(response -> {
		              response.getCause();
		              log.info("Error in :  --> {}",response.getCause());
		            });
	}

}
