package pe.alicorp.services.bridgeazuredirectory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import pe.alicorp.altemista.runner.StarterWebApplication;

@SpringBootApplication
public class BridgeAzureDirectoryApplication extends StarterWebApplication{

	public static void main(String[] args) {
		SpringApplication.run(BridgeAzureDirectoryApplication.class, args);
	}

}
