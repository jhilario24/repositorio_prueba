package pe.alicorp.services.bridgeazuredirectory.service.impl;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import pe.alicorp.services.bridgeazuredirectory.model.request.BridgeRequest;
import pe.alicorp.services.bridgeazuredirectory.model.response.BridgeSuccesResponse;
import pe.alicorp.services.bridgeazuredirectory.proxy.AzureCloudProxy;
import pe.alicorp.services.bridgeazuredirectory.service.BridgeAzureService;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class BridgeAzureServiceImpl implements BridgeAzureService{

	AzureCloudProxy proxy;
	@Override
	public Mono<BridgeSuccesResponse> demandAzure(BridgeRequest request) {
//		 log.info("Ejecuta service. --> {}",request);
//		BridgeSuccesResponse success = new BridgeSuccesResponse();
//		success.setAccessToken("");
//		success.setExpires_on("asasa");
//		return Mono.just(success);
		
		String tenantId= request.getTenant();
		log.info("Succesfull response obtain... -> {}");
		return proxy.requestToAzure(request, tenantId)
				.doOnSuccess(consumer -> {
					log.info("Succes response obtain... -> {}");
				});
	}

}
