package pe.alicorp.services.bridgeazuredirectory.service;

import pe.alicorp.services.bridgeazuredirectory.model.request.BridgeRequest;
import pe.alicorp.services.bridgeazuredirectory.model.response.BridgeSuccesResponse;
import reactor.core.publisher.Mono;

public interface BridgeAzureService {
	public Mono<BridgeSuccesResponse> demandAzure(BridgeRequest request);
	

}
