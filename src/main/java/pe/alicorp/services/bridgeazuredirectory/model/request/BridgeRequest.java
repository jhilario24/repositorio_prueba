package pe.alicorp.services.bridgeazuredirectory.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BridgeRequest {
//	{ 
//		  "tenant":"f567b0f5-6365-4b7a-8579-c9300006c8a4",
//		  "code":"AQABAAIAAACQN9QBRU3jT6bcBQLZNUj7uNcAPNdsvVhsvF56bBadyMrRGUHNZePumZezyd7LvK22yjoaidYmsp8pK6m0MVWm44kQAjgTbXjEyMZbnuYQ63o-OQ5fh6-bsUhMaU5U5n8kgdDA2TzG5dok-_fsgQDyMpY0S3EoZUALaFJKlkauxHPrCqbv5fpf-JE-vo-MoEJqqHLgX1qc4SnFH3Gwn_yIxkY18SJf6MiIeHNuUFit3uXajDamu6S4FJ-fLZ4CdLH6ayiW_ofEyXLK8xx_YKDzbUgfIw0uawnQUhJDDn08tfzseCw5m8Ze5b_cG_D3WoWYWoPVTGKKOu6uKRh7YkVozHdGwHQTndlh6vNMC_ofzJhJfTuSmXy9e3lY1HkBJ-QVbxRIwDoBPSiSR9U6EmM_gkYsNhGj4xivLD19uwesW4xC2_vOqp_m0gvEFYrO-DwSq4CEt2K6TfFPbachjlbT9bfjpZ1eHovBEYUXzZ02nwcW2kZIpPWGhbkZ5uAWMhLxb1p_nV8hWPoA9ZFPkvlvIRYR-a20quTN6Gbf4yh8b5TlvhV2Ir5WmR56FyFRCukgAA",
//		  "client_id":"84f74cc6-d9fd-4df2-ad6c-ae4515ee593e",
//		  "grant_type":"authorization_code",
//		  "redirect_uri":"http://localhost:4200/login-callback.html"
//		}
	private String tenant; 
	private String code;
	private String client_id;
	private String grant_type;
	private String redirect_uri;
	
//	private String tenant; 
//	private String code;
//	private String clientId;
//	private String grantType;
//	private String redirectUri;

}
